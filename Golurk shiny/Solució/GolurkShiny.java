package projectes;

import java.util.Scanner;

public class GolurkShiny {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		final int maxhp = 382;
		int balls = 0;
		boolean atrapat = false;
		boolean ded = false;
		int dmg = sc.nextInt();
		int currhp = maxhp - dmg;

		if (currhp > 0) {
			System.out.println(currhp);
			while (!atrapat && !ded) {
				String poke = sc.next();
				switch (poke) {
				case "pokeball":
					atrapat = pokeball(maxhp, currhp, 12);
					if (!atrapat)
						System.out.println("Fallada");
					balls++;
					break;
				case "superball":
					atrapat = pokeball(maxhp, currhp, 8);
					if (!atrapat)
						System.out.println("Fallada");
					balls++;
					break;
				case "ultraball":
					atrapat = pokeball(maxhp, currhp, 5);
					if (!atrapat)
						System.out.println("Fallada");
					balls++;
					break;
				case "atacar":
					dmg = sc.nextInt();
					currhp = currhp - dmg;
					if (currhp <= 0)
						ded = true;
					else
						System.out.println(currhp);
					break;
				}
			}
		}
		if (currhp < 0 || ded)
			System.out.println("Golurk s'ha debilitat");
		if (atrapat)
			System.out.println(balls);

		sc.close();
	}

	// pokeball es per calcular si se atrapa o no
	static boolean pokeball(int hp, int hpcurr, int ball) {
		int hpmax = hp * 255 * 4;
		int hpball = hpcurr * ball;
		if (hpmax / hpball >= 300)
			return true;
		else
			return false;
	}
}
